import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "csv_to_sql.settings")

application = get_wsgi_application()
