import csv
from core.models import Country, Region, City
from postgres_copy import CopyManager, CopyMapping
from django.core.management.base import BaseCommand
#import pandas as pd


def run():
     with open('..\data\countries.csv', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=',', doublequote=False)
        Region.objects.all().delete()
        Country.objects.all().delete()
        for row in reader:
            if len(row[0].split(','))>5: _, created = Country.objects.get_or_create(
                id = row[0].split(',')[0].replace('"',''),
                title=row[0].split(',')[1] + row[0].split(',')[4] + row[0].split(',')[5],
                )
            elif len(row[0].split(','))>4: _, created = Country.objects.get_or_create(
                 id = row[0].split(',')[0].replace('"',''),
                 title= row[0].split(',')[1] + row[0].split(',')[4],
                 )
            elif len(row[0].split(','))>1: _, created = Country.objects.get_or_create(
                 id = row[0].split(',')[0].replace('"',''),
                 title=row[0].split(',')[1],
                 )
     with open('..\data\sregions.csv', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=',', quotechar='"')
        Region.objects.all().delete()
        for row in reader:
            if len(row[0].split(','))>1:_, created = Region.objects.get_or_create(
                country = Country.objects.get(id=row[0].split(',')[1].replace('"','')),
                title=row[0].split(',')[2],)
    
    #  c = CopyMapping(
    #         # Give it the model
    #         City,
    #         # The path to your CSV
    #         '..\data\_cities.csv',
    #         # And a dict mapping the  model fields to CSV headers
    #         dict(title='title_ru'),
    #     )
    #     # Then save it.
    #  c.save()
    

    # df=pd.read_csv('test_csv.txt',sep=',')



    # row_iter = df.iterrows()

    # objs = [

    #     myClass_in_model(

    #         title = row['title_ru'],

    #     )

    #     for index, row in row_iter

    # ]

    # City.objects.bulk_create(objs)
     


    
            # creates a tuple of the new object or
            # current object and a boolean of if it was created